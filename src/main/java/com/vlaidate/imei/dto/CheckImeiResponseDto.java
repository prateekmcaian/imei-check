package com.vlaidate.imei.dto;

import lombok.Data;

@Data
public class CheckImeiResponseDto {
    boolean valid;
    String replaceLastDigitWith;
}
