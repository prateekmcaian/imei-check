package com.vlaidate.imei.dto;


import lombok.Data;

@Data
public class ImeiDto {

    String imeiNo;
}
