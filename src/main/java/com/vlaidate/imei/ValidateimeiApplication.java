package com.vlaidate.imei;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ValidateimeiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ValidateimeiApplication.class, args);
	}

}
