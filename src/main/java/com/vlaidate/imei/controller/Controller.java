package com.vlaidate.imei.controller;

import com.vlaidate.imei.dto.CheckImeiResponseDto;
import com.vlaidate.imei.dto.ImeiDto;
import com.vlaidate.imei.service.ImeiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/imei")
@CrossOrigin
public class Controller {
    @Autowired
    ImeiService imeiService;

    @PostMapping("/check")
    public CheckImeiResponseDto checkImei(@RequestBody ImeiDto imeiDto){
        boolean validImei = imeiService.isValidImei(imeiDto.getImeiNo());
        CheckImeiResponseDto responseDto=new CheckImeiResponseDto();
        responseDto.setValid(validImei);
        if(!validImei){
            responseDto.setReplaceLastDigitWith(" Last digit replace with "+imeiService.replaceLastDigitWith(imeiDto.getImeiNo()));
        }
        else responseDto.setReplaceLastDigitWith("Don't need to replace with any digit . ");
        return responseDto;
    }
}
